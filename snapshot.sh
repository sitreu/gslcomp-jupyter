#!/bin/bash

user=`whoami`
snapshot_filename=`date +"%F_%H%M"`
snapshot_dir="/home/$user/www/gslcomp-jupyter/${snapshot_filename}"
mkdir -p ${snapshot_dir}
cp *.ipynb ${snapshot_dir}
echo "copied snapshot of all .ipynb files to ${snapshot_dir}"
echo "notebook_viewer_url: https://nbviewer.jupyter.org/url/www.pik-potsdam.de/~${user}/gslcomp-jupyter/${snapshot_filename}"

