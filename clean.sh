#/bin/bash

./snapshot.sh

for file in ./*.ipynb
do
  jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace $file
done

